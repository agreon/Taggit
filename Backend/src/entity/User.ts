import * as bcrypt from "bcrypt-nodejs";
import { IsEmail } from "class-validator";
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Document } from "./Document";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    @IsEmail()
    public email: string;

    @OneToMany(type => Document, document => document.author)
    public documents: Document[];

    @CreateDateColumn()
    public created: Date;

    @Column({
        select: false,
    })
    private password: string;

    // generating a hash
    public setPassword(password) {
        this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8));
    }

    public getPassword() {
        return this.password;
    }
}
