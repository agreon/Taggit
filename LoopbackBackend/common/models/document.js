'use strict';

module.exports = (Document) => {
    Document.beforeRemote('create', (context, user, next) => {
        context.args.data.date = Date.now();
        //context.args.data.userId = context.req.accessToken.userId;
        next();
    });
};
