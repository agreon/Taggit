'use strict';

module.exports = (app) => {
    const ds = app.dataSources.postgres;
    const lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];
    ds.automigrate(lbTables, (err) => {
        if (err) throw err;
        console.log('Loopback tables [' + lbTables + '] created');
    });
};
