'use strict';

module.exports = (app) => {
    app.dataSources.postgres.automigrate('Document', (err) => {
        if (err) throw err;
        app.models.Document.create([{
            name: 'Test-Doc',
        }], (err, documents) => {
            if (err) throw err;
            console.log('Created', documents);
        });
    });
};
